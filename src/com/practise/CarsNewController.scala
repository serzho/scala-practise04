package com.practise

import javax.faces.bean.{RequestScoped, ManagedProperty, SessionScoped, ManagedBean}
import scala.reflect.BeanProperty

@ManagedBean(name="carsNewController")
@RequestScoped
class CarsNewController {

  @BeanProperty
  @ManagedProperty(value="#{car}")
  var car: Car = new Car

  def create: String = {
    CarRepository.save(car)
    "index?faces-redirect=true"
  }
}
