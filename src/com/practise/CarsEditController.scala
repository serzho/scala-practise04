package com.practise

import javax.faces.bean.{RequestScoped, ManagedProperty, ManagedBean}
import scala.reflect.BeanProperty

@ManagedBean(name="carsEditController")
@RequestScoped
class CarsEditController {

  @ManagedProperty(value="#{car}")
  @BeanProperty
  var car: Car = null

  def find(id: String): Car = {
    if (car.id == 0 && id != "")
      car = CarRepository.find(id.toInt).getOrElse(new Car)
    car
  }

  def update: String = {
    CarRepository.save(car)
    "index?faces-redirect=true"
  }
}
