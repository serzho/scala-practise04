package com.practise {

import javax.persistence._
import scala.collection.JavaConversions._

class CarRepository private ()

object CarRepository {

  var mng: EntityManager = _

  def manager = {
    if (mng == null)
      mng = Persistence.createEntityManagerFactory("default").createEntityManager
    mng
  }

  def save(car: Car) {
    manager.getTransaction.begin()
    car.getId match {
      case 0 => manager.persist(car)
      case _ => manager.merge(car)
    }
    manager.getTransaction.commit()
  }

  def find(id: Int): Option[Car] = {
    Option(manager.find(classOf[Car], id))
  }

  def findByModel(model: String) : List[Car] = {
    val query = manager.createQuery("select c from Car c where c.model = :model", classOf[Car])
    query.setParameter("model", model)
    query.getResultList().toList
  }

  def findByYear(year: Int) : List[Car] = {
    val query = manager.createQuery("select c from Car c where c.year = :year", classOf[Car])
    query.setParameter("year", year)
    query.getResultList().toList
  }

  def findByNumber(number: String) :Option[Car] = {
    val query = manager.createQuery("select c from Car c where c.number = :number", classOf[Car])
    query.setParameter("number", number)
    query.getResultList().toList.headOption
  }

  def delete(car: Car) {
    manager.getTransaction.begin()
    val id = car.id
    find(id) foreach { manager.remove(_) }
    manager.getTransaction.commit()
  }

  def deleteAll {
    all.foreach { delete(_) }
  }

  def all: List[Car] = {
    val query = manager.createQuery("select c from Car c", classOf[Car])
    query.getResultList().toList
  }
}
}

