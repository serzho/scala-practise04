package com.practise

import javax.faces.bean.{RequestScoped, ManagedBean, SessionScoped}
import scala.reflect.BeanProperty

@ManagedBean(name="carsIndexController")
@RequestScoped
class CarsIndexController {

  @BeanProperty
  var cars: Array[Car] = CarRepository.all.toArray
}
